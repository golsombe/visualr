require 'rails/generators/dashboard.rb'
require 'rails/generators/chart.rb'


module Visualr
  module Generators # :nodoc:
    class Base < Rails::Generators::NamedBase # :nodoc:
      # Set the current directory as base for the inherited generators.
      def self.base_root
        File.dirname(__FILE__)
      end
    end
  end
end
